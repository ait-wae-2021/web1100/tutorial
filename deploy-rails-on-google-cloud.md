# How to Deploy Rails Application on Google Cloud 

## Register Google Cloud for Free Credit
1. Go to https://cloud.google.com
2. Click button "Get started for free"

![Register](./deploy-rails-on-google-cloud/01.png)

3. Complete the register forms


## Setup VM Instance
1. Go to VM instances page

![GoToInstancePage](./deploy-rails-on-google-cloud/03.png)

2. Click "CREATE INSTANCE"

![CreateInstance](./deploy-rails-on-google-cloud/04.png)

3. Select the region and zone (price is varied on region and zone)

![RegionAndZone](./deploy-rails-on-google-cloud/05.png)

4. Select the OS and disk space (Ubuntu 20.04 LTS with >= 30 GBs is recommended)
5. Turn-on HTTP and HTTPS traffic (you can turn-off HTTP later after SSL setup for security improvement)

![DiskAndTraffic](./deploy-rails-on-google-cloud/06.png)

6. Create new instance



## Setup Super User and Deploy User

1. At your just created instance, click on "SSH" under "Connect" column. It will popup a SSH connected terminal.

![openssh](./deploy-rails-on-google-cloud/07.png)

2. Create new users for deployment with commands (you can change username as you prefer)

``` bash
sudo adduser deploy
``` 

3. Move user "deploy" to sudoer
   
``` bash
sudo usermod -aG sudo deploy
```


## Setup SSH Credential

### Server Side Configuration
1. Generate new ssh key-pair on you local machine. (you can use your existing one)
2. Create /home/deploy/.ssh/authorized_keys via:
   1. Go to directory /home/deploy via
   2. Create .ssh via
   3. Create authorized_keys via
   4. Change the ".ssh" directory owner and all files in inside to user:group "deploy:deploy"
3. Copy your public key then append it to /home/deploy/.ssh/authorized_key on your cloud VM instance.
4. Setup your ~/.ssh/config in you local machine
5. Try to connect to your cloud VM instance via ssh


### Client Side Configuration
1. Open file ```~/.ssh/config``` in your local with any text editor.
2. Add these lines to that file. (You can change the config in Host and User as you prefer; you can find hostname of VM intances at google cloud VM page)

![find-ip](./deploy-rails-on-google-cloud/08.png)

``` 
Host gcloud-deploy
    HostName 34.124.228.77
    User deploy
    IdentityFile ~/.ssh/<your_public_key_filename>
    ServerAliveInterval 60
```

If everything is done well, you can close the browser-builtin terminal. 
We will move to our terminal.


## Setup Rails Stuffs

### Install Rails
1. Install rbenv

``` bash
sudo apt install git
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
mkdir -p ~/.rbenv/plugins
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
```

2. Install Ruby

``` bash
sudo apt install build-essential libpq-dev libreadline-dev libssl-dev zlib1g-dev libyaml-dev nodejs
rbenv install 3.0.2
echo 'rbenv global 3.0.2' >> ~/.bashrc
source ~/.bashrc
echo 'rbenv global 3.0.2' >> ~/.bashrc
gem install rails
```


### Install Posgresql
1. Install posgresql via apt: 

``` bash
sudo apt install postgresql
``` 

2. Create psql user (as superuser), name it as your deploy user.
``` bash
createuser -s deploy
```


### Install Yarn

``` bash
sudo apt install curl software-properties-common
curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt install -y nodejs
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn
```


## Setup Apache2 and Puma

After you success to setup SSH credential, now ssh to your super user with your local terminal.
Then, use the follow command to update you system and install apache2 

``` bash
sudo apt update && sudo apt install -y apache2 postgresql 
```

### Config Apache2

1. Unlink and remove current configurations

``` bash
cd /etc/apache2/sites-available
sudo a2dissite 000-default default-ssl
sudo rm 000-default.conf default-ssl.conf
```

2. Create a new config file ```project.conf``` for http connection (change project path as your demand)

``` xml 
<VirtualHost *:80>
  ServerName domain-name
  DocumentRoot /home/deploy/project-tmp/current/public
  <Directory /home/deploy/project-tmp/current/public>
    AllowOverride all
    Options -MultiViews
    Require all granted
  </Directory>
  <Location /assets>
    ProxyPass !
  </Location>
  <Location /packs>
    ProxyPass !
  </Location>
  <Location /system>
    ProxyPass !
  </Location>
  <Location /favicon.ico>
    ProxyPass !
  </Location>
  ProxyPass / unix:///home/deploy/project-tmp/shared/tmp/sockets/puma.sock|http://localhost/
  ProxyPassReverse / unix:///home/deploy/project-tmp/shared/tmp/puma.sock|http://localhost/
</VirtualHost>
```

3. Enable proxy
   
``` bash
sudo a2enmod proxy
sudo a2enmod proxy_http
```

4. Enable the new configuration
   
``` bash
sudo a2ensite project.conf
```

### Config Puma
1. Go to ```/etc/systemd/system```
2. Create ```puma.service``` file then add these lines.

``` bash
[Unit]
Description=Puma HTTP Server
After=network.target

[Service]
Type=simple

User=deploy
WorkingDirectory=/home/deploy/project-tmp/current
Environment=RAILS_ENV=production
ExecStart=/home/deploy/.rbenv/shims/bundle exec puma

Restart=always

[Install]
WantedBy=multi-user.target
```

3. Reboot your system with the following commands.

``` bash
sudo systemctl daemon-reload
sudo systemctl restart apache2
```

## Setup Capistrano

As we did in the lab, in your local machine, we can setup Capistrano for efficient deployment by.
1. Add capristrano associated gems to Gemfile

``` ruby
group :development do
  gem 'capistrano-rails'
  gem 'capistrano-rbenv'
  gem 'capistrano-rbenv-install', '1.0.0'
  gem 'capistrano-yarn'
  gem 'capistrano-puma'
  gem 'ed25519'
  gem 'bcrypt_pbkdf'
end
```

2. Install Capistrano

``` bash
bundle install
bundle exec cap install
```

3. Append these lines to Capfile 

``` ruby
require "capistrano/rbenv"
require "capistrano/rbenv_install"
require "capistrano/bundler"
require "capistrano/rails/assets"
require "capistrano/rails/migrations"
require "capistrano/yarn"
require "capistrano/puma"
```

4. Create/edit these file as follows.

``` ruby
# config/deploy.rb

# config valid for current version and patch releases of Capistrano

lock "~> 3.16.0"

set :application, "project-tmp"
set :repo_url, "git@gitlab.com:ait-wae-2021/web1100/web12-project.git"
set :rbenv_type, :user
set :rbenv_ruby, '3.0.2'
set :branch, 'main'

append :linked_files, "config/database.yml", "config/master.key"

append :linked_dirs, "log", "tmp", "public/system", "public/assets", "public/packs", ".bundle"

set :keep_releases, 5

after 'deploy:publishing', 'puma:restart'
```

``` ruby
# config/deploy/production.rb 

server "gcloud-deploy", user: "deploy", roles: %w{app db web}

set :deploy_to, "/home/deploy/project-tmp"

set :yarn_flags, '--production --silent --no-progress --network-timeout 1000000'
```


``` ruby
# config/puma.rb 

# Get Rails environment

rails_env = ENV.fetch("RAILS_ENV") { "development" }
environment rails_env

# If production mode, use two workers with preloaded application

if rails_env == "production"
  workers 2
  preload_app!
  app_dir = File.expand_path("../..", __FILE__)
  bind "unix://#{app_dir}/tmp/sockets/puma.sock"
  state_path "#{app_dir}/tmp/sockets/puma.state"
  bind 'unix:///home/deploy/project-tmp/shared/tmp/sockets/puma.sock'
  directory '/home/deploy/project-tmp/current'
end

# 1-5 threads per worker

threads 1, 5

# Run on port 3000 by default

port ENV.fetch("PORT") { 3000 }

# Specify the server's PID file

pidfile ENV.fetch("PIDFILE") { "tmp/pids/server.pid" }

# Allow puma to be restarted by the "rails restart" command

plugin :tmp_restart

# Activate the control app

activate_control_app
```

5. Follow the error messages (copy database.yml, master.key and create psql database)


## Register Domain Name

My recommendation (cheap) : https://domain.z.com/th/en/


## SSL Config

1. Install LetsEncrypt (in letsencrypt prompt, select 1 (temporary webserver))

``` bash
sudo apt-get install -y letsencrypt
sudo systemctl stop apache2
sudo -i certbot certonly    
sudo systemctl start apache2
```

2. Write ```/etc/apache2/sites-available/project.conf``` and ```/etc/apache2/sites-available/project-ssl.conf``` as follows.

``` xml
<!-- project.conf -->

<VirtualHost *:80>
  ServerName you-domain-name
  <!-- Don't forget / after domain name -->
  Redirect permanent / https://your-domain-name/
</VirtualHost>
```

``` xml
<!-- project-ssl.conf -->
<IfModule mod_ssl.c>
  <VirtualHost _default_:443>
    ServerName domain-name
    DocumentRoot /home/deploy/project-tmp/current/public
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    SSLEngine on
    SSLCertificateFile /etc/letsencrypt/live/domain-name/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/domain-name/privkey.pem
    <Directory /home/deploy/project-tmp/current/public>
      AllowOverride all
      Options -MultiViews
      Require all granted
    </Directory>
    <Location /packs>
      ProxyPass !
    </Location>
    <Location /favicon.ico>
      ProxyPass !
    </Location>
    <Location /assets>
      ProxyPass !
    </Location>
    <Location /system>
      ProxyPass !
    </Location>
    ProxyPass / unix:///home/deploy/project-tmp/shared/tmp/sockets/puma.sock|http://localhost/
    ProxyPassReverse / unix:///home/deploy/project-tmp/shared/tmp/puma.sock|http://localhost/
  </VirtualHost>
</IfModule>
```


3. Restart your service

``` bash
sudo a2enmod ssl
sudo a2dissite 000-default
sudo a2dissite default-ssl
sudo a2ensite studentdb
sudo a2ensite studentdb-ssl
sudo systemctl restart apache2
```


# Reference
1. Web Application and Engineer (AT70.12) course, August 2021 from Asian Institute of Technology.